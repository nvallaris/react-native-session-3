import React, { Component } from 'react';
import { Font } from 'expo';
import { Root, StyleProvider } from 'native-base';

import getTheme from './native-base-theme/components';
import platform from './native-base-theme/variables/platform';

import RootStack from './src/routers/RootStack';

class App extends Component {
  state = {
    fontLoaded: false
  };

  async componentDidMount() {
    await Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf')
    });

    this.setState({ fontLoaded: true });
  }  
  
  render() {
    if (this.state.fontLoaded === false) {
      return null;
    }

    return (
      <StyleProvider style={getTheme(platform)}>
        <Root>
          <RootStack/>
        </Root>
      </StyleProvider>
    );
  }
}

export default App;

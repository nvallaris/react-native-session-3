# React Native - Session 3

This project is a series of 3 sessions that show how to build a mobile app using [React Native][1] and [Expo][2].

Session 3 introduces an example about how to use [NativeBase][3] that provides UI components with native look for Android and IOS platform.

[1]: https://facebook.github.io/react-native/
[2]: https://expo.io/
[3]: https://nativebase.io/
export const dataArray = [
  { 
    title: 'What is React Native?',
    content: 'React Native is a JavaScript framework for writing real, natively rendering mobile applications for iOS and Android.'
  },
  { 
    title: 'What is React?',
    content: 'React is a declarative, efficient, and flexible JavaScript library for building user interfaces. It lets you compose complex UIs from small and isolated pieces of code called “components”.'
  },
  { 
    title: 'What is JavaScript?',
    content: 'JavaScript is a programming language used to make web pages interactive. It is what gives a page life—the interactive elements and animation that engage a user.'
  }
];

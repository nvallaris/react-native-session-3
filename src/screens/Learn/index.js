import React, { Component } from 'react';
import {
  Container,
  Content,
  Accordion,
  Header,
  Body,
  Title,
  Left,
  Button,
  Icon,
  Right
} from "native-base";

import AppHeader from 'src/components/AppHeader';

import { dataArray } from './data';

class LearnScreen extends Component {
  getLeftHeaderComponents = () => {
    return (
      <Left>
        <Button
          transparent
          onPress={() => this.props.navigation.goBack()}
        >
          <Icon name='arrow-back' />
        </Button>
      </Left>
    );
  }

  render() {
    return (
      <Container>
        <AppHeader
          title='LearnFirst'
          leftComponents={this.getLeftHeaderComponents()}
         />

        <Content padder>
          <Accordion dataArray={dataArray} expanded={0}/>
        </Content>
      </Container>
    );
  }
}

export default LearnScreen;

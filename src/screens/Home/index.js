import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import {
  Button,
  Text,
  Container,
  Content
} from 'native-base';

import AppHeader from 'src/components/AppHeader';

class HomeScreen extends Component {
  render() {
    return (
      <Container>

        <AppHeader title='LearnFirst' />

        <Content padder>
          <Button
            success
            onPress={() => this.props.navigation.navigate('LearnScreen')}
            style={{ alignSelf: 'center', marginTop: 20 }}
          >
            <Text uppercase={false}>Start Learning</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default HomeScreen;
 
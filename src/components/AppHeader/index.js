import React from 'react';
import {
  Header,
  Body,
  Left,
  Right,
  Title
} from 'native-base';

const populateLeftComponents = (leftComponents) => {
  if (leftComponents === null) {
    return <Left />;
  }

  return leftComponents;
}

const populateRightComponents = (rightComponents) => {
  if (rightComponents === null) {
    return <Right />;
  }

  return rightComponents;
}

const AppHeader = ({
  title = null,
  leftComponents = null,
  rightComponents = null,
}) => {
  return(
    <Header>
      {populateLeftComponents(leftComponents)}
      <Body>
        <Title>{title}</Title>
      </Body>
      {populateRightComponents(rightComponents)}
    </Header>
  );
};

export default AppHeader;